window.onload = function () {
  //<editor-fold desc="Changeable Configuration Block">

  // the following lines will be replaced by docker/configurator, when it runs in a docker-container
  window.ui = SwaggerUIBundle({
    urls: [
      {
        url: "https://buildingintelligence.s3.amazonaws.com/api/sv3_visitor_api_1.0.1.2.yaml",
        name: "SV3 Visitor API",
      },
      { url: "./files/sv3_visitor_api_paramount.yml", name: "Paramount Global - SV3 Visitor" },
    ],
    dom_id: "#swagger-ui",
    deepLinking: true,
    presets: [SwaggerUIBundle.presets.apis, SwaggerUIStandalonePreset],
    plugins: [SwaggerUIBundle.plugins.DownloadUrl],
    layout: "StandaloneLayout",
    supportedSubmitMethods: [],
  });

  //</editor-fold>
};
